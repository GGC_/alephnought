#ifndef GAME_H_
#define GAME_H_

#include <defines.h>
#include <game_types.h>

typedef struct GameState_T {
	f32 deltaTime;
} GameState;

bool gameInitialize(struct Game_T *gameInstance);

bool gameUpdate(struct Game_T *gameInstance, f32 deltaTime);

bool gameRender(struct Game_T *gameInstance, f32 deltaTime);

void gameOnResize(struct Game_T *gameInstance, u32 width, u32 height);

#endif // GAME_H_
