#include "game.h"

#include <entry.h>
#include <core/aleph_memory.h>

bool
createGame(Game *outGame)
{
	outGame->config.startPosX = 200;
	outGame->config.startPosY = 200;
	outGame->config.startWidth = 800;
	outGame->config.startHeight = 600;
	outGame->config.name = "AlephNought - Sandbox";

	outGame->initialize = gameInitialize;
	outGame->update = gameUpdate;
	outGame->render = gameRender;
	outGame->onResize = gameOnResize;

	outGame->state = alephAllocate(sizeof(GameState), MEMORY_TAG_GAME);

	return true;
}
