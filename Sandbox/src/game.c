#include "game.h"
#include <core/logger.h>

bool
gameInitialize(struct Game_T *gameInstance)
{
	ALEPH_DEBUG("game initialize called!");
	return true;
}

bool
gameUpdate(struct Game_T *gameInstance, f32 deltaTime)
{
	return true;
}

bool
gameRender(struct Game_T *gameInstance, f32 deltaTime)
{
	return true;
}

void
gameOnResize(struct Game_T *gameInstance, u32 width, u32 height)
{
	ALEPH_DEBUG("game onresize called!");
}
