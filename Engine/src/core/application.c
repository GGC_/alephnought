#include "application.h"
#include "core/logger.h"
#include "platform/platform.h"
#include "core/aleph_memory.h"
#include "core/event.h"
#include "core/input.h"

#include "game_types.h"

typedef struct ApplicationState_T {
	Game* gameInstance;
	bool isRunning;
	bool isSuspended;
	PlatformState platformState;
	i16 width;
	i16 height;
	f64 lastTime;
} ApplicationState;

static bool appInitialized = false;
static ApplicationState appState;

bool applicationOnEvent(u16 code, void* sender, void* listener_inst, EventContext context);
bool applicationOnKey(u16 code, void* sender, void* listener_inst, EventContext context);

bool
applicationCreate(struct Game_T *gameInstance)
{
	if (appInitialized) {
		ALEPH_ERROR("Application initialized more than once!");
		return false;
	}

	appState.gameInstance = gameInstance;

	initializeLogging();
	initializeInput();

	appState.isRunning = true;
	appState.isSuspended = false;

	if (!eventInitialize()) {
		ALEPH_ERROR("Failed to initialize the event system!");
		return false;
	}

	eventRegister(EVENT_CODE_APPLICATION_QUIT, 0, applicationOnEvent);
	eventRegister(EVENT_CODE_KEY_PRESSED, 0, applicationOnKey);
	eventRegister(EVENT_CODE_KEY_RELEASED, 0, applicationOnKey);

	if (!platformStartup(
			&appState.platformState,
			gameInstance->config.name,
			gameInstance->config.startPosX,
			gameInstance->config.startPosY,
			gameInstance->config.startWidth,
			gameInstance->config.startHeight)) {
		return false;
	}

	if (!appState.gameInstance->initialize(appState.gameInstance)) {
		ALEPH_FATAL("Failed to initialize the game!");
	}

	appState.gameInstance->onResize(appState.gameInstance, appState.width, appState.height);

	appInitialized = true;
	return true;
}

bool
applicationRun()
{
	char* memInfo = alephGetMemoryUsageStr();
	ALEPH_INFO(memInfo);
	platformFree(memInfo, false);

	while (appState.isRunning) {
		if(!platformPumpMessages(&appState.platformState))
			appState.isRunning = false;

		if (!appState.isSuspended) {
			if (!appState.gameInstance->update(appState.gameInstance, (f32)0)) {
				ALEPH_FATAL("Game update failed! Shutting down...");
				appState.isRunning = false;
				break;
			}

			if (!appState.gameInstance->render(appState.gameInstance, (f32)0)) {
				ALEPH_FATAL("Game render failed! Shutting down...");
				appState.isRunning = false;
				break;
			}

			inputUpdate(0);
		}
	}

	appState.isRunning = false;

	eventUnregister(EVENT_CODE_APPLICATION_QUIT, 0, applicationOnEvent);
	eventUnregister(EVENT_CODE_KEY_PRESSED, 0, applicationOnKey);
	eventUnregister(EVENT_CODE_KEY_RELEASED, 0, applicationOnKey);

	eventShutdown();

	platformShutdown(&appState.platformState);

	return true;
}

bool
applicationOnEvent(u16 code, void* sender, void* listenerInstance, EventContext context)
{
	switch (code) {
		case EVENT_CODE_APPLICATION_QUIT: {
			ALEPH_INFO("EVENT_CODE_APPLICATION_QUIT recieved, shutting down.\n");
			appState.isRunning = false;
			return true;
		}
	}

	return true;
}

bool
applicationOnKey(u16 code, void* sender, void* listenerInstance, EventContext context)
{
	if (code == EVENT_CODE_KEY_PRESSED) {
		u16 key_code = context.data.u16[0];
		if (key_code == KEY_ESCAPE) {
			// NOTE: Technically firing an event to itself, but there may be other listeners.
			EventContext data = {};
			eventDispatch(EVENT_CODE_APPLICATION_QUIT, 0, data);

			// Block anything else from processing this.
			return true;
		} else {
			ALEPH_DEBUG("'%c' key pressed in window.", key_code);
		}
	} else if (code == EVENT_CODE_KEY_RELEASED) {
		u16 key_code = context.data.u16[0];
		if (key_code == KEY_B) {
			// Example on checking for a key
			ALEPH_DEBUG("Explicit - B key released!");
		} else {
			ALEPH_DEBUG("'%c' key released in window.", key_code);
		}
	}
	return false;
}
