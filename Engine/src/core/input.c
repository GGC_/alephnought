#include "input.h"

#include "core/event.h"
#include "core/aleph_memory.h"
#include "core/logger.h"
#include <stdbool.h>

typedef struct KeyboardState_T {
	bool keys[MAX_KEYS];
} KeyboardState;

typedef struct MouseState_T {
	i16 x;
	i16 y;

	u8 buttons[MAX_BUTTONS];
} MouseState;

typedef struct InputState_T {
	KeyboardState keyboardCurrent;
	KeyboardState keyboardPrevious;
	MouseState mouseCurrent;
	MouseState mousePrevious;
} InputState;

static bool inputInitialized = false;
static InputState inputState = {};

bool
initializeInput()
{
	// alephZeroMemory(&inputState, sizeof(inputState));
	inputInitialized = true;
	ALEPH_INFO("Input subsystem is initialized");

	return true;
}

bool
shutdownInput()
{
	// TODO: just don't forget to add shutdown routines later lmao
	inputInitialized = false;
	return true;
}

bool
inputUpdate(f64 deltaTime)
{
	if (!inputInitialized)
		return false;

	alephCopyMemory(&inputState.keyboardPrevious, &inputState.keyboardCurrent, sizeof(KeyboardState));
	alephCopyMemory(&inputState.mousePrevious, &inputState.mouseCurrent, sizeof(MouseState));

	return true;
}

void
inputProcessKey(Keys key, bool pressed)
{
	if (inputState.keyboardCurrent.keys[key] != pressed) {
		ALEPH_DEBUG("Key changed: %d", key);
		inputState.keyboardCurrent.keys[key] = pressed;

		EventContext context;
		context.data.u16[0] = key;
		eventDispatch(pressed ? EVENT_CODE_KEY_PRESSED : EVENT_CODE_KEY_RELEASED, 0, context);
	}
}

void
inputProcessButton(Buttons button, bool pressed)
{
	if (inputState.mouseCurrent.buttons[button] != pressed) {
		ALEPH_DEBUG("Mouse button changed: %d", button);
		inputState.mouseCurrent.buttons[button] = pressed;

		EventContext context;
		context.data.u16[0] = button;
		eventDispatch(pressed ? EVENT_CODE_BUTTON_PRESSED : EVENT_CODE_BUTTON_RELEASED, 0, context);
	}
}

void
inputProcessMouseMove(i16 x, i16 y)
{
	if (inputState.mouseCurrent.x != x || inputState.mouseCurrent.y != y) {
		ALEPH_DEBUG("Mouse position: %d, %d", x, y);

		inputState.mouseCurrent.x = x;
		inputState.mouseCurrent.y = y;

		EventContext context;
		context.data.u16[0] = x;
		context.data.u16[1] = y;
		eventDispatch(EVENT_CODE_MOUSE_MOVED, 0, context);
	}
}

void
inputProcessMouseWheel(i8 dz)
{
	EventContext context;
	context.data.i8[0] = dz;
	eventDispatch(EVENT_CODE_MOUSE_WHEEL, 0, context);
}

bool
inputIsKeyDown(Keys key)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.keyboardCurrent.keys[key] == true;
}

bool
inputIsKeyUp(Keys key)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.keyboardCurrent.keys[key] == false;
}

bool
inputWasKeyDown(Keys key)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.keyboardPrevious.keys[key] == true;
}

bool
inputWasKeyUp(Keys key)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.keyboardPrevious.keys[key] == false;
}

bool
inputIsMouseDown(Buttons button)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.mouseCurrent.buttons[button] == true;
}

bool
inputIsMouseUp(Buttons button)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.mouseCurrent.buttons[button] == false;
}

bool
inputWasMouseDown(Buttons button)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.mousePrevious.buttons[button] == true;
}

bool inputWasMouseUp(Buttons button)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		return false;
	}

	return inputState.mousePrevious.buttons[button] == false;
}

void
inputGetMousePosition(i32 *x, i32 *y)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		*x = 0;
		*y = 0;
		return;
	}

	*x = inputState.mouseCurrent.x;
	*y = inputState.mouseCurrent.y;
}

void
inputGetPreviousMousePosition(i32 *x, i32 *y)
{
	if (!inputInitialized) {
		ALEPH_WARN("Input subsystem is not initialized!");
		*x = 0;
		*y = 0;
		return;
	}

	*x = inputState.mousePrevious.x;
	*y = inputState.mousePrevious.y;
}
