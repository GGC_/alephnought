#include "event.h"

#include "core/aleph_memory.h"
#include "logger.h"
#include "containers/darray.h"
#include <stdbool.h>

typedef struct RegisteredEvent_T {
	void *listener;
	PFN_onEvent callback;
} RegisteredEvent;

typedef struct EventCodeEntry_T {
	RegisteredEvent* events;
} EventCodeEntry;

#define MAX_MESSAGE_CODES 0xFFF

typedef struct EventSystemState_T {
	// Lookup Table for Event Codes
	EventCodeEntry registered[MAX_MESSAGE_CODES];
} EventSystemState;

// Event system internal state
static bool isInitialized = false;
static EventSystemState state;

bool
eventInitialize()
{
	if (isInitialized) {
		ALEPH_WARN("Event system already initialized!");
		return false;
	}

	alephZeroMemory(&state, sizeof(state)); // Remove?

	isInitialized = true;
	return true;
}

bool
eventShutdown()
{
	for (u16 i = 0; i < MAX_MESSAGE_CODES; i++) {
		if (state.registered[i].events != 0) {
			darrayDestroy(state.registered[i].events);
			state.registered[i].events = 0;
		}
	}

	return true;
}

bool
eventRegister(u16 code, void *listener, PFN_onEvent onEvent)
{
	if (!isInitialized)
		return false;

	if (state.registered[code].events == 0)
		state.registered[code].events = darrayCreate(RegisteredEvent);

	u64 registeredCount = darrayLength(state.registered[code].events);
	for (u64 i = 0; i < registeredCount; i++) {
		if (state.registered[code].events[i].listener == listener) {
			ALEPH_WARN("Same listener for the event code! Preventing unregistering event twice.");
			return false;
		}
	}

	RegisteredEvent e;
	e.listener = listener;
	e.callback = onEvent;
	darrayPush(state.registered[code].events, e);

	return true;
}

bool
eventUnregister(u16 code, void *listener, PFN_onEvent onEvent)
{
	if (!isInitialized)
		return false;

	if (state.registered[code].events == 0) {
		ALEPH_WARN("Events with code %d is empty!", code);
		return false;
	}

	u64 registeredCount = darrayLength(state.registered[code].events);
	for (u64 i = 0; i < registeredCount; i++) {
		RegisteredEvent e = state.registered[code].events[i];
		if (e.listener == listener && e.callback == onEvent) {
			RegisteredEvent poppedEvent;
			darrayPopAt(state.registered[code].events, i, &poppedEvent);
			return true;
		}
	}

	return false;
}

// TODO: add multithreading and priority list and queue buffers
bool
eventDispatch(u16 code, void *sender, EventContext eventContext)
{
	if (!isInitialized)
		return false;

	if (state.registered[code].events == 0) {
		ALEPH_WARN("Events with code %d is empty!", code);
		return false;
	}

	u64 registeredCount = darrayLength(state.registered[code].events);
	for (u64 i = 0; i < registeredCount; i++) {
		RegisteredEvent e = state.registered[code].events[i];

		// callback can be passed along by not returning true
		if (e.callback(code, sender, e.listener, eventContext)) {
			return true;
		}
	}

	return false;
}
