#include "logger.h"
#include "asserts.h"
#include "platform/platform.h"

// TODO: temporary
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

static const char *levelStirngs[6] = {
	"[TRACE]: ", "[DEBUG]: ", "[INFO]: ", "[WARN]: ", "[ERROR]: ", "[FATAL]: "
};

void
reportAssertionFailure(const char *expression, const char *msg, const char *file, i32 line)
{
	alephLogOutput(LOG_LEVEL_FATAL,
					 "Assertion Failure: %s\n          └── message: %s\n              file: %s, line: %d\n", expression, msg, file, line);
}

bool
initializeLogging()
{
	return true;
}

void
shutdownLogging()
{

}

void
alephLogOutput(LogLevel level, const char* msg, ...)
{
	bool isError = level >= LOG_LEVEL_ERROR;

	const u16 BUFFER_SIZE = 8001;

	char outMessage[BUFFER_SIZE];
	memset(outMessage, 0, sizeof(outMessage));

	va_list argPtr;
	va_start(argPtr, msg);
	vsnprintf(outMessage, 8000, msg, argPtr);
	va_end(argPtr);

	char result[BUFFER_SIZE + 18];
	sprintf(result, "%s%-9s\n", levelStirngs[level], outMessage);

	if (isError)
		platformConsoleWriteError(result, level);
	else
		platformConsoleWrite(result, level);
}
