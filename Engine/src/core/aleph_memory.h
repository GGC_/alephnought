#ifndef ALEPH_MEMORY_H_
#define ALEPH_MEMORY_H_

#include "defines.h"

typedef enum MemoryTag_E {
	MEMORY_TAG_UNKNOWN,
	MEMORY_TAG_ARRAY,
	MEMORY_TAG_DARRAY,
	MEMORY_TAG_DICT,
	MEMORY_TAG_RING_QUEUE,
	MEMORY_TAG_BST,
	MEMORY_TAG_STRING,
	MEMORY_TAG_APPLICATION,
	MEMORY_TAG_JOB,
	MEMORY_TAG_TEXTURE,
	MEMORY_TAG_MATERIAL_INSTANCE,
	MEMORY_TAG_RENDERER,
	MEMORY_TAG_GAME,
	MEMORY_TAG_TRANSFORM,
	MEMORY_TAG_ENTITY,
	MEMORY_TAG_ENTITY_NODE,
	MEMORY_TAG_SCENE,

	MEMORY_TAG_MAX_TAGS
} MemoryTag;

ALEPH_API void initializeMemory();
ALEPH_API void shutdownMemory();

ALEPH_API void* alephAllocate(u64 size, MemoryTag tag);
ALEPH_API void alephFree(void *block, u64 size, MemoryTag tag);
ALEPH_API void* alephZeroMemory(void *block, u64 size);
ALEPH_API void* alephCopyMemory(void *dest, const void *source, u64 size);
ALEPH_API void* alephSetMemory(void *dest, i32 value, u64 size);

ALEPH_API char* alephGetMemoryUsageStr();

#endif // ALEPH_MEMORY_H_
