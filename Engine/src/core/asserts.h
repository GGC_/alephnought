#ifndef ASSERTS_H_
#define ASSERTS_H_

#include <signal.h>
#include "defines.h"

#define ALEPH_ASSERTIONS_ENABLED

#ifdef ALEPH_ASSERTIONS_ENABLED

#if defined(_MSC_VER)
	#include <intrin.h>
	#define DEBUG_BREAK __debugbreak()
#else
	#define DEBUG_BREAK raise(SIGTRAP)
#endif

ALEPH_API void reportAssertionFailure(
	const char* expression, const char* msg, const char* file, i32 line
);

#define ALEPH_ASSERT(expr)												\
	{																	\
		if (!(expr)) {													\
			reportAssertionFailure(#expr, "", __FILE__, __LINE__);	\
			DEBUG_BREAK;												\
		}																\
	}

#define ALEPH_ASSERT_MSG(expr, msg)										\
	{																	\
		if (!(expr)) {													\
			reportAssertionFailure(#expr, msg, __FILE__, __LINE__);	\
			DEBUG_BREAK;												\
		}																\
	}

#ifdef DEBUG
#define ALEPH_ASSERT_DEBUG(expr)										\
	{																	\
		if (!(expr)) {													\
			reportAssertionFailure(#expr, "", __FILE__, __LINE__);	\
			DEBUG_BREAK;												\
		}																\
	}
#else
#define ALEPH_ASSERT_DEBUG(expr)
#endif
#else

#define ALEPH_ASSERT(expr)
#define ALEPH_ASSERT_MSG(expr, msg)
#define ALEPH_ASSERT_DEBUG(expr)

#endif

#endif // ASSERT_H_
