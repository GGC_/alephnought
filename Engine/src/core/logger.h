#ifndef LOGGER_H_
#define LOGGER_H_

#include "defines.h"
#include "core.h"

#define LOG_TRACE_ENABLED 1
#define LOG_DEBUG_ENABLED 1
#define LOG_INFO_ENABLED  1
#define LOG_WARN_ENABLED  1

#ifndef DEBUG
#define LOG_TRACE_ENABLED 0
#define LOG_DEBUG_ENABLED 0
#endif

typedef enum LogLevel_E {
	LOG_LEVEL_TRACE = 0,
	LOG_LEVEL_DEBUG = 1,
	LOG_LEVEL_INFO  = 2,
	LOG_LEVEL_WARN  = 3,
	LOG_LEVEL_ERROR = 4,
	LOG_LEVEL_FATAL = 5,
} LogLevel;

bool initializeLogging();
void shutdownLogging();

ALEPH_API void alephLogOutput(LogLevel level, const char *msg, ...);

#define ALEPH_FATAL(message, ...) alephLogOutput(LOG_LEVEL_FATAL, message, ##__VA_ARGS__)
#define ALEPH_ERROR(message, ...) alephLogOutput(LOG_LEVEL_ERROR, message, ##__VA_ARGS__)

#if LOG_WARN_ENABLED == 1
#define ALEPH_WARN(message, ...) alephLogOutput(LOG_LEVEL_WARN, message, ##__VA_ARGS__)
#else
#define ALEPH_WARN(message, ...)
#endif

#if LOG_INFO_ENABLED == 1
#define ALEPH_INFO(message, ...) alephLogOutput(LOG_LEVEL_INFO, message, ##__VA_ARGS__)
#else
#define ALEPH_INFO(message, ...)
#endif

#if LOG_INFO_ENABLED == 1
#define ALEPH_DEBUG(message, ...) alephLogOutput(LOG_LEVEL_DEBUG, message, ##__VA_ARGS__)
#else
#define ALEPH_DEBUG(message, ...)
#endif

#if LOG_INFO_ENABLED == 1
#define ALEPH_TRACE(message, ...) alephLogOutput(LOG_LEVEL_TRACE, message, ##__VA_ARGS__)
#else
#define ALEPH_TRACE(message, ...)
#endif

#endif // LOGGER_H_
