#include "aleph_memory.h"

#include "core/logger.h"
#include "platform/platform.h"

#include <string.h>
#include <stdio.h>

struct MemoryStats {
	u64 totalAllocated;
	u64 taggedAllocations[MEMORY_TAG_MAX_TAGS];
};

static struct MemoryStats stats;

static const char* memoryTagStrArray[MEMORY_TAG_MAX_TAGS] = {
	"UNKNOWN     ",
	"ARRAY       ",
	"DARRAY      ",
	"DICT        ",
	"RING_QUEUE  ",
	"BST         ",
	"STRING      ",
	"APPLICATION ",
	"JOB         ",
	"TEXTURE     ",
	"MAT_INSTANCE",
	"RENDERER    ",
	"GAME        ",
	"TRANSFORM   ",
	"ENTITY      ",
	"ENTITY_NODE ",
	"SCENE       "
};

void
initializeMemory()
{
	platformZeroMemory(&stats, sizeof(stats));
}

void
shutdownMemory()
{

}

void*
alephAllocate(u64 size, MemoryTag tag)
{
	if (tag == MEMORY_TAG_UNKNOWN)
		ALEPH_WARN("Aleph tries allocating MEMORY_TAG_UNKNOWN. Re-class this allocation.");

	stats.totalAllocated += size;
	stats.taggedAllocations[tag] += size;

	// TODO: memory alignment
	void *block = platformAllocate(size, false);
	platformZeroMemory(block, size);

	return block;
}

void
alephFree(void *block, u64 size, MemoryTag tag)
{
	if (tag == MEMORY_TAG_UNKNOWN)
		ALEPH_WARN("Aleph tries freeing MEMORY_TAG_UNKNOWN. Re-class this deallocation.");

	stats.totalAllocated -= size;
	stats.taggedAllocations[tag] -= size;

	// TODO: memory alignment
	platformFree(block, false);
}

void*
alephZeroMemory(void *block, u64 size)
{
	return platformZeroMemory(block, size);
}

void*
alephCopyMemory(void *dest, const void *source, u64 size)
{
	return platformCopyMemory(dest, source, size);
}

void*
alephSetMemory(void *dest, i32 value, u64 size)
{
	return platformSetMemory(dest, value, size);
}

// TODO: replace so that no malloc (?only print)
char*
alephGetMemoryUsageStr()
{
	const u64 kb = 1024;
	const u64 mb = 1024 * kb;
	const u64 gb = 1024 * mb;

	char buffer[8000] = "System memory use:\n";
	u64 offset = strlen(buffer);

	for (u32 i = 0; i < MEMORY_TAG_MAX_TAGS; i++) {
		char unit[3] = "XB";
		float amount = 1.0f;

		if (stats.taggedAllocations[i] >= gb) {
			unit[0] = 'G';
			amount = stats.taggedAllocations[i] / (float) gb;
		} else if (stats.taggedAllocations[i] >= mb) {
			unit[0] = 'M';
			amount = stats.taggedAllocations[i] / (float) mb;
		} else if (stats.taggedAllocations[i] >= kb) {
			unit[0] = 'K';
			amount = stats.taggedAllocations[i] / (float) kb;
		} else {
			unit[0] = 'B'; unit[1] = '\0';
			amount = (float) stats.taggedAllocations[i];
		}

		i32 len = snprintf(buffer + offset, 8000, "    %s : %.2f%s\n", memoryTagStrArray[i], amount, unit);
		offset += len;
	}

	char* outStr = strdup(buffer);
	return outStr;
}
