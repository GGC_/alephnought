#ifndef APPLICATION_H_
#define APPLICATION_H_

#include "defines.h"

struct Game_T;

typedef struct ApplicationConfig_T {
	i16 startPosX;
	i16 startPosY;

	i16 startWidth;
	i16 startHeight;

	char* name;
} ApplicationConfig;

ALEPH_API bool applicationCreate(struct Game_T *gameInstance);

ALEPH_API bool applicationRun();

#endif // APPLICATION_H_
