#ifndef EVENT_H_
#define EVENT_H_

#include "defines.h"

typedef struct EventContext_T {
	// 128 bits
	union {
		i64 i64[2];
		u64 u64[2];
		f64 f64[2];

		i32 i32[4];
		u32 u32[4];
		f32 f32[4];

		i16 i16[8];
		u16 u16[8];

		i8 i8[16];
		u8 u8[16];

		char c[16];
	} data;
} EventContext;

typedef bool (*PFN_onEvent)(u16 code, void *sender, void *listenerInstance, EventContext data);

bool eventInitialize();
bool eventShutdown();

ALEPH_API bool eventRegister(u16 code, void *listener, PFN_onEvent onEvent);

ALEPH_API bool eventUnregister(u16 code, void *listener, PFN_onEvent onEvent);

ALEPH_API bool eventDispatch(u16 code, void *sender, EventContext eventContext);

// NOTE: For application event codes created by user, use codes >0xFF=255
typedef enum SystemEventCode_E {
	// Shutting the application down on the next frame
	EVENT_CODE_APPLICATION_QUIT = 0x01,

	// Keyboard key pressed
	// u16 key_code = data.data.u16[0];
	EVENT_CODE_KEY_PRESSED = 0x02,

	// Keyboard key pressed
	// u16 key_code = data.data.u16[0];
	EVENT_CODE_KEY_RELEASED = 0x03,

	// Keyboard key pressed
	// u16 button = data.data.u16[0];
	EVENT_CODE_BUTTON_PRESSED = 0x04,

	// Keyboard key pressed
	// u16 button = data.data.u16[0];
	EVENT_CODE_BUTTON_RELEASED = 0x05,

	// Keyboard key pressed
	// u16 x = data.data.u16[0];
	// u16 y = data.data.u16[1];
	EVENT_CODE_MOUSE_MOVED = 0x06,

	// Keyboard key pressed
	// u16 dz = data.data.i8[0];
	EVENT_CODE_MOUSE_WHEEL = 0x07,

	// Keyboard key pressed
	// u16 width = data.data.u16[0];
	// u16 height = data.data.u16[1];
	EVENT_CODE_WINDOW_RESIZED = 0x08,

	MAX_EVENT_CODE = 0xFF
} SystemEventCode;

#endif // EVENT_H_
