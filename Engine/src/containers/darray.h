#ifndef DARRAY_H_
#define DARRAY_H_

#include "defines.h"

/*
 * Memory layout:
 *   - u64 capacity : # of elements that can be held
 *   - u64 length : # of elements contained
 *   - u64 stride : size of each element in bytes
 *   - void *element : point to the elements
 */

enum {
	DARRAY_CAPACITY,
	DARRAY_LENGTH,
	DARRAY_STRIDE,
	DARRAY_FIELD_LENGTH,
};

ALEPH_API void* _darrayCreate(u64 length, u64 stride);
ALEPH_API void _darrayDestroy(void *array);

ALEPH_API u64 _darrayFieldGet(void *array, u64 field);
ALEPH_API void _darrayFieldSet(void *array, u64 field, u64 value);

ALEPH_API void* _darrayResize(void *array);

ALEPH_API void* _darrayPush(void *array, const void *pValue);
ALEPH_API void _darrayPop(void *array, void *dest);

ALEPH_API void* _darrayInsertAt(void *array, u64 index, const void *pValue);
ALEPH_API void* _darrayPopAt(void *array, u64 index, void *dest);

#define DARRAY_DEFAULT_CAPACITY 1
#define DARRAY_RESIZE_FACTOR 2

#define darrayCreate(type) \
	_darrayCreate(DARRAY_DEFAULT_CAPACITY, sizeof(type))

#define darrayReserve(type, capacity) \
	_darrayCreate(capacity, sizeof(type))

#define darrayDestroy(array) \
	_darrayDestroy(array);

// NOTE: cloning involved
#define darrayPush(array, value)		   \
	{									   \
		__auto_type temp = (value);		   \
		array = _darrayPush(array, &temp); \
	}

#define darrayPop(array, pValue) \
	_darrayPop(array, pValue)


#define darrayInsertAt(array, index, value)				   \
	{													   \
		__auto_type temp = (value);						   \
		array = _darrayInsertAt(array, index, &temp);	   \
	}

#define darrayPopAt(array, index, dest) \
	_darrayPopAt(array, index, dest)

#define darrayClear(array) \
	_darrayFieldSet(array, DARRAY_LENGTH, 0)

#define darrayCapacity(array) \
	_darrayFieldGet(array, DARRAY_CAPACITY)

#define darrayLength(array) \
	_darrayFieldGet(array, DARRAY_LENGTH)

#define darrayStride(array) \
	_darrayFieldGet(array, DARRAY_STRIDE)

#define darrayLengthSet(array, len) \
	_darrayFieldSet(array, DARRAY_LENGTH, len)

#endif // DARRAY_H_
