#include "darray.h"

#include "core/aleph_memory.h"
#include "logger.h"

void*
_darrayCreate(u64 length, u64 stride)
{
	u64 headerSize = DARRAY_FIELD_LENGTH * sizeof(u64);
	u64 arraySize = length * stride;
	u64 *newArray = alephAllocate(headerSize + arraySize, MEMORY_TAG_DARRAY);

	alephSetMemory(newArray, 0, headerSize + arraySize);

	newArray[DARRAY_CAPACITY] = length;
	newArray[DARRAY_LENGTH] = 0;
	newArray[DARRAY_STRIDE] = stride;

	return (void*)(newArray + DARRAY_FIELD_LENGTH);
}

void
_darrayDestroy(void *array)
{
	u64 *header = (u64*)array - DARRAY_FIELD_LENGTH;
	u64 headerSize = DARRAY_FIELD_LENGTH * sizeof(u64);
	u64 totalSize = headerSize + header[DARRAY_CAPACITY] * header[DARRAY_STRIDE];
	alephFree(header, totalSize, MEMORY_TAG_DARRAY);
}

u64
_darrayFieldGet(void *array, u64 field)
{
	u64 *header = (u64*)array - DARRAY_FIELD_LENGTH;
	return header[field];
}

void
_darrayFieldSet(void *array, u64 field, u64 value)
{
	u64 *header = (u64*)array - DARRAY_FIELD_LENGTH;
	header[field] = value;
}

void*
_darrayResize(void *array)
{
	u64 len = darrayLength(array);
	u64 stride = darrayStride(array);
	void *temp = _darrayCreate(
		DARRAY_RESIZE_FACTOR * darrayCapacity(array),
		stride
	);

	alephCopyMemory(temp, array, len * stride);

	_darrayFieldSet(temp, DARRAY_LENGTH, len);
	_darrayDestroy(array);

	return temp;
}

void*
_darrayPush(void *array, const void *pValue)
{
	u64 len = darrayLength(array);
	u64 stride = darrayStride(array);

	if (len >= darrayCapacity(array))
		array = _darrayResize(array);

	u64 addr = (u64)array;
	addr += len * stride;
	alephCopyMemory((void*)addr, pValue, stride);
	_darrayFieldSet(array, DARRAY_LENGTH, len+1);

	return array;
}

void*
_darrayInsertAt(void *array, u64 index, const void *pValue)
{
	u64 len = darrayLength(array);
	u64 stride = darrayStride(array);

	if (index >= len) {
		ALEPH_ERROR("Index outisde of the bound of the array! Length: %d, Index: %d", len, index);
		return array;
	}

	if (len >= darrayCapacity(array))
		_darrayResize(array);

	u64 addr = (u64)array;

	if (index != len-1) {
		alephCopyMemory(
			(void*)(addr + (index+1) * stride),
			(void*)(addr + index * stride),
			(len - index) * stride);
	}

	alephCopyMemory((void*)(addr + index * stride), pValue, stride);

	_darrayFieldSet(array, DARRAY_LENGTH, len+1);
	return array;
}

void
_darrayPop(void *array, void *dest)
{
	u64 len = darrayLength(array);
	u64 stride = darrayStride(array);

	u64 addr = (u64) array;
	addr += (len-1) * stride;

	alephCopyMemory(dest, (void*)addr, stride);
	_darrayFieldSet(array, DARRAY_LENGTH, len-1);
}

void*
_darrayPopAt(void *array, u64 index, void *dest)
{
	u64 len = darrayLength(array);
	u64 stride = darrayStride(array);

	if (index >= len) {
		ALEPH_ERROR("Index outisde of the bound of the array! Length: %d, Index: %d", len, index);
		return array;
	}

	u64 addr = (u64)array;
	alephCopyMemory(dest, (void*)(addr + index * stride), stride);

	if (index != len-1) {
		alephCopyMemory(
			(void*)(addr + index * stride),
			(void*)(addr + (index+1) * stride),
			(len - index) * stride);
	}

	_darrayFieldSet(array, DARRAY_LENGTH, len-1);
	return array;
}
