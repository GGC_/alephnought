#ifndef GAME_TYPES_H_
#define GAME_TYPES_H_

#include "core/application.h"

typedef struct Game_T {
	ApplicationConfig config;

	bool (*initialize)(struct Game_T *gameInstance);

	bool (*update)(struct Game_T *gameInstance, f32 deltaTime);

	bool (*render)(struct Game_T *gameInstance, f32 deltaTime);

	void (*onResize)(struct Game_T *gameInstance, u32 width, u32 height);

	// Game-specific state created by the user
	void *state;
} Game;

#endif // GAME_TYPES_H_
