#ifndef ENTRY_H_
#define ENTRY_H_

#include "core/application.h"
#include "core/logger.h"
#include "core/aleph_memory.h"
#include "game_types.h"

extern bool createGame(Game *outGame);

// NOTE: The main entry point of the application
int
main(void)
{
	initializeMemory();

	Game gameInstance;
	if (!createGame(&gameInstance)) {
		ALEPH_FATAL("Could not create a game!");
		return -1;
	}

	if (!gameInstance.update || !gameInstance.render || !gameInstance.initialize || !gameInstance.onResize) {
		ALEPH_FATAL("One of the game's function pointers is not assigned!");
		return -2;
	}

	if (!applicationCreate(&gameInstance)) {
		ALEPH_ERROR("Failed to create the application!");
		return 1;
	}

	if (!applicationRun()) {
		ALEPH_ERROR("Failed to shutdown application normally!");
		return 2;
	}

	shutdownMemory();

	return 0;
}

#endif // ENTRY_H_
