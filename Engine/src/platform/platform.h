#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "defines.h"

typedef struct PlatformState_T {
	void* internalState;
} PlatformState;

bool platformStartup(
	PlatformState *platformState,
	const char *applicationName,
	i32 x, i32 y,
	i32 width, i32 height);

void platformShutdown(PlatformState *platformState);

bool platformPumpMessages(PlatformState *platformState);

// TODO: remove export whenever possible
void* platformAllocate(u64 size, bool aligned);
void platformFree(void *block, bool aligned);
void* platformZeroMemory(void *block, u64 size);
void* platformCopyMemory(void *dest, const void *source, u64 size);
void* platformSetMemory(void *dest, i32 value, u64 size);

void platformConsoleWrite(const char *msg, u8 color);
void platformConsoleWriteError(const char *msg, u8 color);

f64 platformGetAbsoluteTime();

void platformSleep(u64 ms);

#endif
