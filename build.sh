#!/usr/bin/env bash

DEBUG="debug"

if [ "$1" == "$DEBUG" ]; then
	cd Engine && make debug && cd ../Sandbox && sh run.sh
else
	cd Engine && make release && cd ../Sandbox && sh run.sh
fi
